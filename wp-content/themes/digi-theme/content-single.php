<?php 
    if( have_rows("single_content_builder") ):

        // loop through the rows of data
        while ( have_rows("single_content_builder") ) : the_row();
            switch (get_row_layout()) :
                case "post_applications":
                    get_template_part("templates/page_builder/single-builder/applications");
                    break;
            endswitch;
        endwhile;
    else:
        the_content();
    endif;
?>