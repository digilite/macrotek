<?php get_header(); ?>
<?php 
$bg = get_field('products_hero_image','option');
$singletitle = get_field('products_hero_title','option');
$id = get_the_ID();
?>
<section class="hero hero-single">
	<div class="hero-image-container" style="background: url(<?= $bg ?>) no-repeat center center / cover">
	<div class="hero-title-block">
		<h1 class="h1"><?= $singletitle; ?></h1>
	</div>
	</div>
</section>

<div class="container user-content">
	<div class="row">
		<div class="col-md-4">
			<div class="tabs-titles">
				<?php
				global $post;
				$args = array(
					'post_type'  => 'products',
					'posts_per_page' => 20
				);
				$myposts = get_posts( $args ); ?>
				<?php
				foreach( $myposts as $post ){ setup_postdata($post);?>
				<div class="tabs-item" data-id="<?php the_ID(); ?>">
					<a class="tabs-menu-item" href="<?php the_permalink(); ?>/#general-content" class="tab-post-title"><?php the_title(); ?></a>
				</div> 
				<?php }
				wp_reset_postdata();
				?>
			</div>
			<div class="mobile-tabs text-center">
					<p class="mob-tab-title">Select a solution</p>
					<div class="dropdown">
						<p class="btn dropbtn" data-flip="false" id="dropdownMenuButton" aria-haspopup="true" aria-expanded="false">
							<?php the_title(); ?>
						</p>
						<div class="drop-menu" aria-labelledby="dropdownMenuButton">
						    <?php
							global $post;
							$args = array(
								'post_type'  => 'products',
								'posts_per_page' => 20,
								'post__not_in'   => array($id)
							);
							$myposts = get_posts( $args ); ?>
							<?php
							foreach( $myposts as $post ){ setup_postdata($post);?>
								<a class="dropdown-item" href="<?php the_permalink(); ?>/#general-content"><?php the_title(); ?></a>
							<?php }
							wp_reset_postdata();
							?>
						</div>
					</div>
			</div>
		</div>
		<div class="col-md-8 post-single-content user-content" data-id="<?= $id; ?>" id="general-content">
			<h2 class="post-title"><?php the_title(); ?></h2>
			<?php
			if (have_posts()) :
				while (have_posts()) :
				  the_post(); ?>
				  <p><?php the_content(); ?></p><?php
				  endwhile;
				endif; ?>
				<?php get_template_part("content-single"); ?>
				<?php get_template_part("templates/feautured"); ?>
		</div>
	</div>
</div>
<?php 
$bgimage = get_field('product_cta_background', 'option');
$buttonurl = get_field('product_cta_button_url', 'option');
$buttonlable = get_field('product_cta_button_label', 'option');
$ctadesc = get_field('product_cta_desscription', 'option');
$imgmob = get_field('cta_image_for_mobile_pr', 'option');
?>
<section class="cta <?php the_sub_field("cta_block_type"); ?>" style="background: url('<?= $bgimage; ?>') no-repeat center center / cover">
	<div class="container">
		<div class="row justify-content-end">
			<div class="cta-img-mobile col-md-6">
				<img src="<?= $imgmob; ?>" alt="">
			</div>
			<div class="cta-description col-md-6">
				<p><?= $ctadesc; ?></p>
				<a class="btn btn-green" data-toggle="modal" data-target="#exampleModal" href="#"><?= $buttonlable; ?></a>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>