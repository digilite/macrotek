<?php 
    if( have_rows("page_content") ):

        // loop through the rows of data
        while ( have_rows("page_content") ) : the_row();
            switch (get_row_layout()) :
                case "two_column_with_image":
                    get_template_part("templates/page_builder/two_column_with_image");
                    break;
                case "show_posts":
                    get_template_part("templates/page_builder/show_posts");
                    break;
                case "tabs_post":
                    get_template_part("templates/page_builder/tabs");
                    break;
                case "cta_block":
                    get_template_part("templates/page_builder/cta");
                    break;
                case "icons_post":
                    get_template_part("templates/page_builder/icons_post");
                    break;
                case "title_with_logo":
                    get_template_part("templates/page_builder/title_logo");
                    break;
            endswitch;
        endwhile;
    else:
        the_content();
    endif;
?>