<?php get_header(); ?>
<?php 
$banner = get_field('studies_single_hero_image', 'options');
$id = get_the_ID();
?>
<section class="case-hero-single">
	<div class="container">
		<img src="<?= $banner;?>" alt="thumbnail">
	</div>
</section>

<div class="container">
	<div class="row">
		<div class="col-md-9 post-single-content user-content" data-id="<?= $id; ?>">
			<?php
			if (have_posts()) :
				while (have_posts()) :
					the_post(); ?>
					<h2 class="post-title"><?php the_title(); ?></h2>
					<?php the_content(); ?>
				<?php endwhile;
			endif; ?>
		</div>
		<div class="col-md-3">
			<?php get_template_part("templates/feautured"); ?>
		</div>
		<a href="http://localhost/macrotek/case_studies/" class="btn btn-white more-projects">Learn more projects</a>
	</div>
</div>
<?php get_template_part("templates/contact_form"); ?>

<?php get_footer(); ?>