<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="format-detection" content="telephone=no">
	<title><?php echo wp_get_document_title(); ?></title>
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<!-- Generate favicon here http://www.favicon-generator.org/ -->
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<header  itemscope itemtype="http://schema.org/WPHeader">
		<div class="container flex-container header-items">
			<div class="container-burger" onclick="myFunction(this)">
				<div class="bar1"></div>
				<div class="bar2"></div>
				<div class="bar3"></div>
            </div>
			<div itemscope itemtype="http://schema.org/Organization" id="logo">
				<a itemprop="url" href="<?php echo bloginfo('url') ?>">
					<?php $logo = get_field('site_logo_general', 'option'); ?>
					<img src="<?= $logo; ?>" alt="logo">
				</a>
			</div>
			<nav itemscope itemtype="http://schema.org/SiteNavigationElement"><?php
				wp_nav_menu([
					'theme_location' => 'primary-menu',
					'menu_class' => 'main-menu flex-container',
					'container' => '',
				]); ?>
			</nav>
			<div class="spare-parts">
				<a href="#" data-toggle="modal" data-target="#exampleModal" class="btn btn-white">Contact a Representative</a>
				<img src="<?php echo get_template_directory_uri(); ?>/img/ctai.svg" alt="asset" data-toggle="modal" data-target="#exampleModal"/> 
			</div>
		</div>
	</header>