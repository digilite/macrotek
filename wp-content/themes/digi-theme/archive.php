<?php get_header(); 
$bg = get_field('news_hero_image','option');
$archivetitle = get_field('news_hero_title','option');
?>
<section class="hero hero-single">
	<div class="hero-image-container" style="background: url(<?= $bg ?>) no-repeat center center / cover">
	<div class="hero-title-block">
		<h1 class="h1"><?= $archivetitle; ?></h1>
	</div>
	</div>
</section>
<div class="container">
	<div class="row news-archive relative-parent">
		<?php
		if (have_posts()) :
			while (have_posts()) :
				the_post(); ?>
				<div class="post-item item news col-md-6 withouth_desc">
					<a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url( 'large' );?>" alt=""></a>
					<a href="<?php the_permalink(); ?>"><h3 class="post-title"><?php the_title(); ?></h3></a>
					<a class="more" href="<?php the_permalink(); ?>">Read more ></a>
				</div>
			<?php
			endwhile;
		endif; ?>
		<?php pagination_nav(); ?>
	</div>
</div>

<?php get_footer(); ?>