var $ = jQuery;
var single_slide_bullets_fade = {
  loop: true,
  dots: true,
    autoplay: true,
    lazyLoad: true,
    margin:100,
  autoplayTimeout: 7000,
  responsiveClass: true,
    responsive:{
    0:{
        items:2,
        center: true
    },
    768 : {
      items:3,
    },
    992:{
        items:4,
    },
}
};

var single_slide_bullets = {
	loop: true,
  dots: true,
    autoplay: true,
    lazyLoad: true,
    margin:100,
  autoplayTimeout: 7000,
  responsiveClass: true,
    responsive:{
    0:{
        items:3,
        center: true
    },
    768 : {
      items:4,
    },
    992:{
        items:4,
    },
}
};


var products_slide = {
  loop: true,
  dots: true,
    autoplay: true,
    lazyLoad: true,
    margin:24,
  autoplayTimeout: 7000,
  responsiveClass: true,
    responsive:{
    0:{
        items:3,
        center: true
    },
    768 : {
      items:1,
    },
    992:{
        items:1,
    },
}
};


var slider_section = $('.icon-post .row');
var post_application = $(".applications-slide .row");
var product_application = $('.products-slide .row');

function myFunction(x) {
    x.classList.toggle("change");
    document.getElementById("mob-menu").toggle("show");
}

$(document).ready(function() {

  $(".download-file").each(function(){
    var hr = $(this).attr("href");
    $(this).attr("data-src", hr);
    });
  
    $(".download-file").removeAttr("href");
  
    function checkstorage() {
  
    if(localStorage.downloadform == "submited") {
    $(".download-file").attr("download","");
    $(".download-file").removeAttr("data-toggle");
    $(".download-file").removeAttr("data-target");
    $(".download-file").css("pointer-events", "all");
    $(".download-file").each(function(){
      var src = $(this).attr("data-src");
      $(this).attr("href", src);
      });
  } else {
    $(".download-file").attr("data-toggle", "modal");
      $(".download-file").attr("data-target", "#downladform");
  }
  
  }
  
  checkstorage();
  
  $(document).on("frmFormComplete", function (event, form, response) {
    localStorage.downloadform = "submited";
    $(".download-file").removeAttr("data-toggle");
    $(".download-file").removeAttr("data-target");
    $(".download-file").attr("download", "true");
    $(".download-file").css("pointer-events", "all");
    $(".download-file").each(function(){
      var src = $(this).attr("data-src");
      $(this).attr("href", src);
      }); 
  });
  
  
  
  
  $(".download-file").click(function() {
    if(localStorage.downloadform == "submited") {
      $(".download").removeAttr("data-toggle");
      $(".download").removeAttr("data-target");
    }
  })
  
  $("#logo").click(function() {
    localStorage.clear();
  })







    var str = window.location.href;
    str = str.split("/");

   $(".menu-item").each(function(){
      var str1 = $(this).children('a').attr("href");
      str1 = str1.split("/");
      if(str[str.length - 3] == str1[str1.length - 3]) {
        $(this).css('border-bottom', '2px solid #0A416A');
      }
   });

  $("#owl-demo").owlCarousel({
    nav : true,
    navText: ["<img src='wp-content/themes/digi-theme/img/prev.svg'>","<img src='wp-content/themes/digi-theme/img/next.svg'>"],
    dots: true,
    loop: true,
    autoplay: true,
    controls: true,
    slideSpeed : 300,
    paginationSpeed : 400,
    items : 1, 
    itemsDesktop : false,
    itemsDesktopSmall : false,
    itemsTablet: false,
    itemsMobile : false

});


  $("[href=#contact_us]").attr({
    "data-target":"#exampleModal",
    "data-toggle":"modal"
  })

  $('.dropbtn').click(function(){
    $('.drop-menu').slideToggle();
    $(this).toggleClass('brdbtn');
    $('.drop-menu').toggleClass('brdtop');
  })

    $(".container-burger").click(function(){
        $("#mob-menu").toggleClass("show");
    })

    var alterClass = function() {
        var ww = document.body.clientWidth;
        if (ww < 991) {
          if($('.post-application div.item').length >= 3) {
            post_application.owlCarousel(single_slide_bullets);
            product_application.owlCarousel(products_slide);
          } else {}
          $('.related .row').addClass('owl-carousel owl-theme');
          slider_section.owlCarousel(single_slide_bullets_fade);
        } else if (ww >= 601) {
          $('.post-application .row').removeClass('owl-carousel owl-theme');
        };
      };
      $(window).resize(function(){
        alterClass();
      });
      //Fire it when the page first loads:
      alterClass();

    $('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });








    $('.owl-carousel').owlCarousel({
        loop: true,
	    dots: true,
        autoplay: true,
        lazyLoad: true,
        margin:24,
	    autoplayTimeout: 7000,
	    responsiveClass: true,
        responsive:{
        0:{
            items:1
        },
        768 : {
          items:3,
        },
        992:{
            items:4,
        },
    }
    })
    
    var tab_item = $(".home .tabs-titles .tabs-item");
    var tab_id;
    $(".home .tabs-content:first-child").addClass("show-content");
    $(".home .tabs-titles .tabs-item:first-child").addClass("active-tab");

    tab_item.click(function(){
        tab_item.removeClass("active-tab");
        $(this).addClass("active-tab");
        tab_id = $(this).attr("data-id");
        $('.home .tabs-content.show-content').removeClass("show-content");
        $('.home .tabs-content[data-id="'+ tab_id +'"]').addClass("show-content");
    })

    $('.footer-menu-title').click(function(){
      $(this).next('.footer-menu').slideToggle();
      $(this).toggleClass('arrtop');
    })
    


    tab_content_id = $(".post-single-content").attr("data-id");

    $('.single .tabs-item[data-id="'+ tab_content_id +'"]').addClass("active");


    $("#burger-icon").click(function(){
        $(this).toggleClass("open");
        $("#site-nav").addClass("opend");
    });

		

	//if(testimonials_slider.length) {
		//testimonials_slider.owlCarousel(single_slide_bullets_fade);
	//}
    $(".user-content iframe").each(function(){
        $(this).wrap("<div class='embed-responsive embed-responsive-16by9'></div>")
    });
    mobileMenu();
    
    
});

function mobileMenu() {
    if ($(window).width() < 992 ) {
        $(".menu-item-has-children > a").click(function(e){
            e.preventDefault();
            $(this).toggleClass("clicked");
            $(this).parent().find(".sub-menu").slideToggle();
        });
    }
}