<?php $isoicon = get_field('iso_icon', 'option'); ?>
<img class="iso-logo" src="<?= $isoicon; ?>" alt="iso">
<?php
while (have_rows("social_links", "option")):
	the_row();
	if (get_sub_field("link", "option")): ?>
			<a href="<?php the_sub_field("link"); ?>" target="_blank" class="transition social-footer">
				<?php the_sub_field("icon", "option"); ?>
			</a>
		<?php
	endif;
endwhile;