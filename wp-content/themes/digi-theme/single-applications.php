<?php get_header(); ?>
<?php 
$bg = get_field('app_hero_image','option');
$singletitle = get_field('app_hero_title','option');
$id = get_the_ID();
?>
<section class="hero hero-single">
	<div class="hero-image-container" style="background: url(<?= $bg ?>) no-repeat center center / cover">
	<div class="hero-title-block">
			<h1 class="h1"><?= $singletitle; ?></h1>
	</div>
	</div>
</section>

<div class="container">
	<div class="row">
		<div class="col-md-4">
			<div class="tabs-titles">
				<?php
				global $post;
				$args = array(
					'post_type'  => 'applications',
					'posts_per_page' => 20
				);
				$myposts = get_posts( $args ); ?>
				<?php
				foreach( $myposts as $post ){ setup_postdata($post);?>
				<div class="tabs-item" data-id="<?php the_ID(); ?>">
					<a class="tabs-menu-item" href="<?php the_permalink(); ?>/#general-content" class="tab-post-title"><?php the_title(); ?></a>
				</div> 
				<?php }
				wp_reset_postdata();
				?>
			</div>
			<div class="mobile-tabs text-center">
					<p class="mob-tab-title">Select a solution</p>
					<div class="dropdown">
						<p class="btn dropbtn" data-flip="false" id="dropdownMenuButton" aria-haspopup="true" aria-expanded="false">
							<?php the_title(); ?>
						</p>
						<div class="drop-menu" aria-labelledby="dropdownMenuButton">
						    <?php
							global $post;
							$args = array(
								'post_type'  => 'applications',
								'posts_per_page' => 20,
								'post__not_in'   => array($id)
							);
							$myposts = get_posts( $args ); ?>
							<?php
							foreach( $myposts as $post ){ setup_postdata($post);?>
								<a class="dropdown-item" href="<?php the_permalink(); ?>/#general-content"><?php the_title(); ?></a>
							<?php }
							wp_reset_postdata();
							?>
						</div>
					</div>
			</div>
		</div>
		<div class="col-md-8 post-single-content user-content" data-id="<?= $id; ?>" id="general-content">
			<?php
			if (have_posts()) :
				while (have_posts()) :
					the_post(); ?>
					<div class="flex-container app-single-heading">
					<?php $icon = get_field('application_icon');
			            echo file_get_contents( $icon ); ?>
						<h2 class="post-title app-single-title"><?php the_title(); ?></h2>
					</div>
					<?php the_content(); ?>
					<?php get_template_part("content-single"); ?>
				<?php endwhile;
			endif; ?>
			<?php get_template_part("templates/feautured"); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>