<?php 
$cformimage = get_field('conact_form_image','option');
?>
<section class="contact-form" id="contact-form-smooth">
	<div class="container">
		<h3 class="block-title contact-mobile-title">Contact a Representative</h3>
		<div class="row">
			<div class="col-md-6">
				<img class="contact-form-image" src="<?= $cformimage; ?>" alt="">
			</div>
			<div class="col-md-6">
				<h3 class="block-title">Contact a Representative</h3>
				<?php echo do_shortcode("[formidable id=1 ajax='true']"); ?>
			</div>
		</div>
	</div>
</section>


