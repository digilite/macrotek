<?php 
$post_thumbnail_id = get_post_thumbnail_id();
$bg = wp_get_attachment_image_url( $post_thumbnail_id ,'large' );
$herostyle = get_field("hero_style");
$herotitle = get_field("hero_title");
$herobutton = get_field("hero_button_lable");
$herobuttonurl = get_field("button_url");
?>
<section class="hero <?= $herostyle; ?>">
	<div class="hero-slide owl-carousel owl-theme" id="owl-demo">
	       <?php 
			if( have_rows('hero_slide') ):
    		while( have_rows('hero_slide') ) : the_row();
			   $slideimg = get_sub_field('hero_slide_image');
			   $slidetitle = get_sub_field('hero_slide_title');
			   $slidebutton= get_sub_field('hero_button_lable');
			   $slideurl = get_sub_field('button_url');
			   $slidsubtitle = get_sub_field('hero_slide_subtitle');
			   $textcolor =  get_sub_field('slide_title_color'); ?>
			   <div class="item" class="hero-image-container <?php the_field("centered"); ?>" style="background: url(<?= $slideimg; ?>) no-repeat center center / cover">
					<div class="container">
						<?php
						if($herostyle == "homepage-hero") : ?>
						<div class="hero-info">
							<?php if($slidetitle) : $titlenone = ''; ?>
							<h1  class="h1"><?= $slidetitle; ?></h1>
							<?php else: $titlenone = 'title-none'; endif; ?>
							<?php if($slideurl) : ?>
							<a href="<?= $slideurl; ?> <?= $titlenone; ?>" class="btn btn-green"><?= $slidebutton; ?></a>
							<?php endif; ?>
							<?php if($slidsubtitle) : ?>
							<h2 class="slide-subtitle <?php the_sub_field('slide_title_color'); ?> <?= $titlenone; ?>"><?= $slidsubtitle; ?></h2>
							<?php endif; ?>
						</div>
						<?php else : endif; ?>	
					</div>
				</div>
    			<?php	endwhile; else : endif; ?>
	</div>
	<div class="company-info flex-container">
		<?php
					if( have_rows('hero_about_us') ):
						while( have_rows('hero_about_us') ) : the_row();
							$icon = get_sub_field('hero_about_icon');
							$number = get_sub_field('hero_about_number');
							$text = get_sub_field('hero_about_text'); ?>
							<div class="info-item">
								<div class="count-icon">
									<img src="<?= $icon; ?>" alt="">
									<p class="number text-left"><?= $number; ?> + </p>
								</div>
								<p class="name"><?= $text ?></p>
							</div>   
			<?php endwhile;
		else : endif; ?>
	</div>
</section>
