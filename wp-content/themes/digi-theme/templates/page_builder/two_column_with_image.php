<?php 
$image = get_sub_field('two_col_image');
$image_title = get_sub_field('two_col_image_title');
$description = get_sub_field('two_col_description');
$desctitle = get_sub_field('two_description_title'); ?>
<section class="two-column">
	<div class="container">
		<div class="row justify-content-between">	
			<div class="col-md-4 image-column">
				<img src="<?= $image; ?>" alt="">
				<p class="image-title text-center"><?= $image_title; ?></p>
			</div>
			<div class="col-md-6 two-col-description">
				<div class="two-column-title flex-container">
					<h2><?= $desctitle; ?></h2>
				</div>
				<?= $description; ?>
			</div>
		</div>
	</div>
</section>


