<section class="show-post in-solution">
	<div class="container">
		<h3 class="block-title">industry Solutions</h3>
		<div class="row post-slider owl-carousel owl-theme">
				<?php
				global $post;
				$args = array(
					'post_type'  => 'industry-solutions',
					'posts_per_page' => 10
				);
				$myposts = get_posts( $args ); ?>
				<?php
				foreach( $myposts as $post ){ setup_postdata($post); ?>
				<div class="post-item item in-solution-item withouth_desc">
					<img src="<?php the_post_thumbnail_url( 'large' );?>" alt="">
					<h3 class="post-title"><?php the_title(); ?></h3>
					<a class="more" href="<?php the_permalink(); ?>">Read more ></a>
				</div>
				<?php }
				wp_reset_postdata();
				?>
		</div>
	</div>
</section>


