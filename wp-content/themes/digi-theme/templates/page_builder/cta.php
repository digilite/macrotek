<?php 
$bgimage = get_sub_field('cta_image');
$buttonurl = get_sub_field('cta_button_url');
$buttonlable = get_sub_field('cta_button');
$ctadesc = get_sub_field('cta_description');
$imgmob = get_sub_field('cta_image_for_mobile');
?>
<section class="cta <?php the_sub_field("cta_block_type"); ?>" style="background: url('<?= $bgimage; ?>') no-repeat, #D3DFD8;">
	<div class="container">
		<div class="row justify-content-end">
			<div class="cta-img-mobile col-md-6">
				<img src="<?= $imgmob; ?>" alt="">
			</div>
			<div class="cta-description col-md-6">
				<p><?= $ctadesc; ?></p>
				<a class="btn btn-green" data-toggle="modal" data-target="#exampleModal" href="#"><?= $buttonlable; ?></a>
			</div>
		</div>
	</div>
</section>


