<?php 
$post_type = get_sub_field('select_tabs_post_type');
$block_title = get_sub_field('tabs_post_title');
$pgurl = get_sub_field('tabs_title_url');
?>
<section class="tab-post">
	<div class="container">
		<a href="<?= $pgurl; ?>"><h3 class="block-title"><?= $block_title; ?></h3></a>
		<div class="row justify-content-between">
			<div class="col-md-3 tabs-titles">
				<?php
				global $post;
				$args = array(
					'post_type'  => $post_type,
					'posts_per_page' => 6
				);
				$myposts = get_posts( $args ); ?>
				<?php
				foreach( $myposts as $post ){ setup_postdata($post);?>
				<div class="tabs-item" data-id="<?php the_ID(); ?>">
					<h3 class="tab-post-title"><?php the_title(); ?></h3>
				</div> 
				<?php }
				wp_reset_postdata();
				?>
			</div>
			<div class="col-md-8">
			    <?php foreach( $myposts as $post ){ setup_postdata($post);?>
				<div class="tabs-content" data-id="<?php the_ID(); ?>">
					<h2 class="post-title"><?php the_title(); ?></h2>
					<?php the_content(); ?>
					<?php get_template_part("content-single"); ?>
				</div> 
				<?php }
				wp_reset_postdata();
				?>
			</div>
		</div>
	</div>
</section>
<?php get_template_part("templates/page_builder/solution-mobile"); ?>


