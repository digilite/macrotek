<?php 
$post_type = get_sub_field('select_icons_post_type');
$block_title = get_sub_field('icons_post_title');
$purl = get_sub_field('application_title_url');
?>
<section class="icon-post">
	<div class="container">
		<a href="<?= $purl; ?>"><h3 class="block-title text-center"><?= $block_title; ?></h3></a>
		<div class="row justify-content-between standart-padding">
				<?php
				global $post;
				$args = array(
					'post_type'  => $post_type,
					'posts_per_page' => 6
				);
				$myposts = get_posts( $args ); ?>
				<?php
				foreach( $myposts as $post ){ setup_postdata($post);?>
				<div class="icon-post-item text-center item">
					<a class="full-click" href="<?php the_permalink(); ?>"></a>
					<?php $icon = get_field('application_icon'); ?>
					<a href="<?php the_permalink(); ?>"><img src="<?= $icon; ?>" alt=""></a>
					<a href="<?php the_permalink(); ?>" class="post-title"><?php the_title(); ?></a>
				</div>
				<?php }
				wp_reset_postdata();
				?>
		</div>
	</div>
</section>


