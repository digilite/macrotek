<?php
$categories = get_the_category();
$category_id = $categories[0]->cat_ID;
$post_type = get_sub_field('single_select_post_type');
$page_object = get_queried_object();
if($post_type == 'applications') :
$cls = 'applications-slide';
$featured_posts = get_sub_field('select_application');
$itemclass = "post-app-item";
$blocktitle = "See the following applications for more details.";
$afterdesc = get_sub_field('after_description');
elseif($post_type == 'products') : 
$featured_posts = get_sub_field('select_products');
$blocktitle = "Macrotek’s removal technologies include:";
$itemclass = "post-product-item";
$cls = "products-slide";
else:
endif;
?>
<section class="post-application <?= $cls; ?> <?= $category_id; ?>">
	<p class="app-content-title"><?= $blocktitle; ?></p>
	<div class="row standart-padding">
	<?php
if( $featured_posts ): ?>
    <?php foreach( $featured_posts as $featured_post ): 
        $permalink = get_permalink( $featured_post->ID );
		$title = get_the_title( $featured_post->ID );
		$icon = get_field('application_icon', $featured_post->ID);
        ?>
		<div class="<?= $itemclass; ?> item">
			<?php if( $post_type == 'applications' ): ?>
			<a class="full-click" href="<?= $permalink; ?>"></a>
			<a href="<?= $permalink; ?>"><img class="app-thumb" src="<?= $icon;?>" alt=""></a>
			<?php else: $thumb = get_the_post_thumbnail_url( $featured_post->ID, 'thumbnail' );?>
			<a href="<?= $permalink; ?>"><img src="<?= $thumb; ?>" alt="" class="th"></a>
			<?php endif; ?>
			<a class="app-title" href="<?= $permalink; ?>"><?= $title; ?></a>
		</div>
    <?php endforeach; ?>
<?php endif; ?>
	</div>
	<p class="after-post"><?= $afterdesc; ?></p>
</section>


