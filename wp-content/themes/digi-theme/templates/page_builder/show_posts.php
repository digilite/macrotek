<?php 
$post_type = get_sub_field('select_post_type');
$block_title = get_sub_field('show_post_title');
$carousel = get_sub_field('enable_carousel');
$columns = get_sub_field('columns');
$pageurl = get_sub_field('sw_title_url');
$post_per_page = 20;
if($carousel) {
	$col = "";
	$padding = "";
} else {
	$col = $columns;
}
if($post_type == 'post') {
	$cat = 'news';
	$post_per_page = 2;
	$catg = 'newscat';
}
?>
<section class="show-post <?= $catg; ?> <?= $post_type; ?>">
	<div class="container">
		<a href="<?= $pageurl; ?>"><h3 class="block-title"><?= $block_title; ?></h3></a>
		<div class="row post-slider <?php the_sub_field('enable_carousel'); ?>">
				<?php
				global $post;
				$args = array(
					'post_type'  => $post_type,
					'posts_per_page' => $post_per_page,
					'cat' => $cat
					
				);
				$myposts = get_posts( $args ); ?>
				<?php
				foreach( $myposts as $post ){ setup_postdata($post);
				$shortdesc = get_field('short_description');
				if($shortdesc) : $withouth = "with_desc"; else: $withouth = "withouth_desc"; endif; ?>
				<div class="post-item item <?= $cat; ?> <?= $col; ?> <?= $padding; ?> <?= $withouth; ?>">
					<a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url( 'large' );?>" alt=""></a>
					<a href="<?php the_permalink(); ?>"><h3 class="post-title"><?php the_title(); ?></h3></a>
					<?php if($shortdesc) : ?>
					<a href="<?php the_permalink(); ?>"><p class="short-description"><?= $shortdesc; ?></p></a>
					<?php else : endif; ?>
					<a class="more" href="<?php the_permalink(); ?>">Read more ></a>
				</div>
				<?php }
				wp_reset_postdata();
				?>
		</div>
		<a class="read-more-news" href="<?php echo bloginfo('url') ?>/category/news">Read more news</a>
		<a class="read-more-news read-more-studies" href="<?php echo bloginfo('url') ?>/case_studies">Read more projects</a>
	</div>
</section>


