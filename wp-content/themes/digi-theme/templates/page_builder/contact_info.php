<?php 
$bgimage = get_field('background_contact_opt','option');
$adress = get_field('adress_opt','option');
$phone = get_field('phone_opt','option');
$email = get_field('email','option');
$showinfo = get_field('show_contact_info');
$cformimage = get_field('conact_form_image','option');
if( $showinfo && in_array('contact_info', $showinfo) ) :
?>
<section class="contact-info" style="background: url('<?= $bgimage; ?>') no-repeat center center / cover">
	<div class="container">
		<div class="row">
		<div class="contact_us text-center">
			<h3 class="block-title">Contact Us</h3>
			<p class="conact_adress"><i class="fas fa-map-marker-alt"></i><?= $adress; ?></p>
			<p class="conact_phone"><i class="fas fa-phone-alt"></i><?= $phone; ?></p>
			<p class="conact_email"><i class="fas fa-envelope"></i><?= $email; ?></p>
		</div>
		</div>
	</div>
</section>
<?php else : endif; ?>


