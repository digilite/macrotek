<?php 
$titletwl = get_sub_field('twl_title');
?>
<section class="twl-section" style="background-color: #D3DFD8;">
	<div class="container">
		<div class="row justify-content-center">
			<img src="<?php echo get_template_directory_uri(); ?>/img/logo_circle.svg" alt="">
			<h2 class="twl-title"><?= $titletwl; ?></h2>
		</div>
	</div>
</section>


