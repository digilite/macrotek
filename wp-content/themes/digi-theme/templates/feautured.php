<?php
$id = get_the_ID();
$category = get_post_type($id);
if($category == 'products'):
$post_type = 'case_studies';
$ftdurl = 'case_studies';
elseif($category == 'industry-solutions'):
$post_type = 'post';
$ftdurl = 'category/news';
elseif($category = 'applications'):
$post_type = 'case_studies';
$ftdurl = 'case_studies';
elseif($category = 'products'):
$post_type = 'case_studies';
$ftdurl = 'case_studies';
endif;
?>
<section class="related <?= $category; ?>">
		<h3 class="block-title">Related Materials</h3>
		<div class="row justify-content-between standart-padding">
				<?php
				global $post;
				$args = array(
					'post_type'  => $post_type,
					'posts_per_page' => 3
				);
				$myposts = get_posts( $args ); ?>
				<?php
				foreach( $myposts as $post ){ setup_postdata($post);?>
				<div class="feautured-post-item feautured-<?= $post_type; ?>">
					<a href="<?php the_permalink(); ?>" class="post-title"><img src="<?php the_post_thumbnail_url( 'large' );?>" alt=""></a>
					<a href="<?php the_permalink(); ?>" class="post-title"><?php the_title(); ?></a>
					<a class="more" href="<?php the_permalink(); ?>">Read more ></a>
				</div>
				<?php }
				wp_reset_postdata();
				?>
		</div>
		<a class="read-more-news" href="<?php echo bloginfo('url') ?>/<?= $ftdurl; ?>">Read more projects</a>
</section>


