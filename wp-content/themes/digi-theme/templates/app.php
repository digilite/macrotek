<section class="icon-post">
	<div class="container">
		<h3 class="block-title text-center">Applications</h3>
		<div class="row justify-content-between standart-padding">
				<?php
				global $post;
				$args = array(
					'post_type'  => 'applications',
					'posts_per_page' => 6
				);
				$myposts = get_posts( $args ); ?>
				<?php
				foreach( $myposts as $post ){ setup_postdata($post);
				$iconi = get_field('application_icon'); ?>
				<div class="icon-post-item text-center">
					<a class="full-click" href="<?php the_permalink(); ?>"></a>
					<a href="<?php the_permalink(); ?>"><img src="<?= $iconi; ?>" alt=""></a>
					<a href="<?php the_permalink(); ?>" class="post-title"><?php the_title(); ?></a>
				</div>
				<?php }
				wp_reset_postdata();
				?>
		</div>
	</div>
</section>


