<?php get_header(); 
/**
* Template Name: Homepage
*
* @package WordPress
* @subpackage Digilite Theme
*/
?>
<?php get_template_part("templates/hero"); ?>
<?php get_template_part("content"); ?>
<?php get_template_part("templates/page_builder/contact_info"); ?>
<?php get_footer(); ?>