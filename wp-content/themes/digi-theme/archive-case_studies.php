<?php get_header();
$bg = get_field('cs_archive_page_hero_image','option');
$archivetitle = get_field('cs_archive_page_hero_title','option');

?>
<section class="hero hero-single">
	<div class="hero-image-container" style="background: url(<?= $bg ?>) no-repeat center center / cover">
	<div class="hero-title-block">
		<h1 class="h1"><?= $archivetitle; ?></h1>
	</div>
	</div>
</section>
<section class="post-archive">
	<div class="container">
		<div class="row">
			<?php
			global $wp_query;
			$args = array_merge( $wp_query->query_vars, ['posts_per_page' => 4 ] );
			query_posts( $args );
			if (have_posts()) :
				while (have_posts()) :
					the_post(); ?>
					<div class="post-item item col-md-3 withouth_desc">
							<a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url( 'large' );?>" alt=""></a>
							<a href="<?php the_permalink(); ?>"><h3 class="post-title"><?php the_title(); ?></h3></a>
							<a class="more" href="<?php the_permalink(); ?>">Read more ></a>
						</div>
				<?php endwhile;
			endif; ?>
		</div>
		<h2 class="section-title">Featured Projects</h2>
		<div class="row">
				<?php
				global $wp_query;
				$args1 = array_merge( $wp_query->query_vars, ['posts_per_page' => 8]);
				query_posts( $args1 );
				if (have_posts()) :
					while (have_posts()) :
						the_post(); 
						?>
						<div class="post-item item col-md-3 withouth_desc feautured-case">
							<a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url( 'large' );?>" alt=""></a>
							<a href="<?php the_permalink(); ?>"><h3 class="post-title"><?php the_title(); ?></h3></a>
							<a class="more" href="<?php the_permalink(); ?>">Read more ></a>
						</div>
					<?php endwhile;
				endif; ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>