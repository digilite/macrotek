<?php get_header(); 
/**
* Template Name: Spare Parts
*
* @package WordPress
* @subpackage Digilite Theme
*/

$bg = get_field('spare_hero_image','option');
$archivetitle = get_field('spare_hero_title','option');

?>
<section class="hero spare-hero">
	<div class="hero-image-container" style="background: url(<?= $bg ?>) no-repeat center center / cover">
	<div class="hero-title-block-spare">
		<h1 class="h1"><?= $archivetitle; ?></h1>
	</div>
	</div>
</section>
<div class="container spare-parts-form"><?php
	if (have_posts()) :
		while (have_posts()) :
			the_post(); ?>
			<?php the_content(); ?><?php
		endwhile;
	endif; ?>
</div>
<?php get_template_part("templates/page_builder/contact_info"); ?>
<?php get_footer(); ?>