<?php
// ADD CUSTOM POST TYPE IF NEEDED

function custom_post_type_products() {
	$labels = [
		"name"               => _x("Products", "products"),
		"singular_name"      => _x("Products", "product"),
		"add_new"            => _x("Add New", "Product"),
		"add_new_item"       => __("Add New Product"),
		"edit_item"          => __("Edit Product"),
		"new_item"           => __("New Product"),
		"all_items"          => __("All Product"),
		"view_item"          => __("View Product"),
		"search_items"       => __("Search Products"),
		"not_found"          => __("No Products found"),
		"not_found_in_trash" => __("No Products found in the Trash"), 
		"parent_item_colon"  => "",
		"menu_name"          => "Products"
	];

	$args = [
		"labels"        => $labels,
		"public"        => true,
		"menu_position" => 5,
		"has_archive"   => true,
		"show_in_rest" => true,
        "supports" => array('editor', 'title','thumbnail', 'post-formats' ,'custom-fields')
	];
	register_post_type("products", $args);
}
add_action("init", "custom_post_type_products");


function custom_post_type_ins() {
	$labels = [
		"name"               => _x("Industry Solutions", "industry-solutions"),
		"singular_name"      => _x("Industry Solution", "industry-solution"),
		"add_new"            => _x("Add New", "Industry Solution"),
		"add_new_item"       => __("Add New Industry Solution"),
		"edit_item"          => __("Edit Industry Solution"),
		"new_item"           => __("New Industry Solution"),
		"all_items"          => __("All Industry Solutions"),
		"view_item"          => __("View Industry Solution"),
		"search_items"       => __("Search Industry Solutions"),
		"not_found"          => __("No Industry Solutions found"),
		"not_found_in_trash" => __("No Industry Solutions found in the Trash"), 
		"parent_item_colon"  => "",
		"menu_name"          => "Industry Solutions"
	];

	$args = [
		"labels"        => $labels,
		"public"        => true,
		"menu_position" => 5,
		"supports"      => ["title", "editor", "thumbnail", "post-formats" ,"custom-fields"],
		"has_archive"   => true,
		"show_in_rest" => true,
        "supports" => array('editor')
	];
	register_post_type("industry-solutions", $args);
}
add_action("init", "custom_post_type_ins");


function custom_post_type_applications() {
	$labels = [
		"name"               => _x("Applications", "applications"),
		"singular_name"      => _x("Application", "application"),
		"add_new"            => _x("Add New", "Application"),
		"add_new_item"       => __("Add New Application"),
		"edit_item"          => __("Edit Application"),
		"new_item"           => __("New Application"),
		"all_items"          => __("All Applications"),
		"view_item"          => __("View Application"),
		"search_items"       => __("Search Applications"),
		"not_found"          => __("No Applications found"),
		"not_found_in_trash" => __("No Applications found in the Trash"), 
		"parent_item_colon"  => "",
		"menu_name"          => "Applications"
	];

	$args = [
		"labels"        => $labels,
		"public"        => true,
		"menu_position" => 5,
		"has_archive"   => true,
		"show_in_rest" => true,
        "supports" => array('editor', 'title','thumbnail', 'post-formats' ,'custom-fields')
	];
	register_post_type("applications", $args);
}
add_action("init", "custom_post_type_applications");


function custom_post_type_case_studies() {
	$labels = [
		"name"               => _x("Case Studies", "case_studies"),
		"singular_name"      => _x("Case Studie", "case_studie"),
		"add_new"            => _x("Add New", "Case Studie"),
		"add_new_item"       => __("Add New Case Studie"),
		"edit_item"          => __("Edit Case Studie"),
		"new_item"           => __("New Case Studie"),
		"all_items"          => __("All Case Studies"),
		"view_item"          => __("View Case Studie"),
		"search_items"       => __("Search Case Studies"),
		"not_found"          => __("No Case Studies found"),
		"not_found_in_trash" => __("No Case Studies found in the Trash"), 
		"parent_item_colon"  => "",
		"menu_name"          => "Case Studies"
	];

	$args = [
		"labels"        => $labels,
		"public"        => true,
		"menu_position" => 5,
		"has_archive"   => true,
		"show_in_rest" => true,
        "supports" => array('editor', 'title','thumbnail', 'post-formats' ,'custom-fields')
	];
	register_post_type("case_studies", $args);
}
add_action("init", "custom_post_type_case_studies");
