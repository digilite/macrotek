<?php get_header(); ?>
<section class="case-hero-single">
	<div class="container">
		<img src="<?php the_post_thumbnail_url( 'large' );?>" alt="thumbnail">
	</div>
</section>
<section class="post-content user-content">
	<div class="container"><?php
		if (have_posts()) :
			while (have_posts()) :
				the_post(); ?>
				<h2 class="post-title"><?php the_title(); ?></h2>
				<?php the_content(); ?><?php
			endwhile;
		endif; ?>
		<div class="row">
			<?php
			$args = array(
				'posts_per_page' => 5
			);
			
			$query = new WP_Query( $args );
			
			// Цикл
			if ( $query->have_posts() ) {
				while ( $query->have_posts() ) {
					$query->the_post(); ?>
					<div class="post-item item news col-md-6 withouth_desc">
						<img src="<?php the_post_thumbnail_url( 'large' );?>" alt="">
						<h3 class="post-title"><?php the_title(); ?></h3>
						<a class="more" href="<?php the_permalink(); ?>">Read more ></a>
					</div>
				<?php }
			} else {
				// Постов не найдено
			}
			// Возвращаем оригинальные данные поста. Сбрасываем $post.
			wp_reset_postdata();?>
		</div>
		<a class="read-more-news" href="<?php echo bloginfo('url') ?>/category/news">Read more news</a>
	</div>
</section>
<?php get_template_part("templates/contact_form"); ?>
<?php get_footer(); ?>