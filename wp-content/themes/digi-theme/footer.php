<?php 
$footerlogo = get_field('footer_logo', 'option');
$adress = get_field('adress_opt','option');
$email = get_field('email','option');
$footerphone = get_field('footer_phone','option');
$isoicon = get_field('iso_icon', 'option');
?>
		<footer itemscope itemtype="http://schema.org/WPFooter">
			<div class="container">
				<div class="row justify-content-between">
					<div class="footer-logo col-md-3">
						<a itemprop="url" href="<?php echo bloginfo('url') ?>">
							<img src="<?= $footerlogo; ?>" alt="logo">
						</a>
						<p class="contact_adress_footer contact-footer"><i class="fas fa-map-marker-alt"></i><?= $adress; ?></p>
						<p class="contact_phone_footer contact-footer"><i class="fas fa-phone-alt"></i><?= $footerphone; ?></p>
						<p class="contact_email_footer contact-footer"><i class="fas fa-envelope"></i><?= $email; ?></p>
						<?php get_template_part("social"); ?>
					</div>
					<div class="footer-menu-block col-md-2">
						<?php wp_nav_menu([
						'theme_location' => 'footer-position-1',
						'menu_class' => 'footer-menu footer-menu-1',
						'container' => '',
						]); ?>
					</div>
					<div class="contact-image col-md-2">
						<img src="<?php echo get_template_directory_uri(); ?>/img/ftmessage.png" title="">
					</div>
					<div class="footer-form col-md-5">
							<h4>Contact a Representative</h4>
							<?php echo do_shortcode('[formidable id=1 ajax="true"]'); ?>
					</div>
				</div>
			</div>
		</footer>
		<?php wp_footer(); ?>
		<div class="mobile-menu text-center" id="mob-menu">
			<nav itemscope itemtype="http://schema.org/SiteNavigationElement"><?php
				wp_nav_menu([
					'theme_location' => 'primary-menu',
					'menu_class' => 'main-menu',
					'container' => '',
				]); ?>
			</nav>
			<a href="#" data-toggle="modal" data-target="#exampleModal" class="btn btn-green">Contact a Representative</a>
		</div>
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title" id="exampleModalLabel">Contact a Representative</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<?php echo do_shortcode('[formidable id=1 ajax="true"]'); ?>
				</div>
				</div>
			</div>
        </div>

		<div class="modal fade" id="downladform" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title" id="exampleModalLabel">Subscribe and download</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<?php echo do_shortcode('[formidable id=4 ajax="true"]'); ?>
				</div>
				</div>
			</div>
        </div>
	</body>
</html>